Require a rust version `>= 1.44.0` (tested with 1.44.0).
```
cd ocaml
opam switch create . 4.09.1
eval $(opam config env)
cd ../
./build_deps.sh
cd ocaml
opam install . --deps-only
```
