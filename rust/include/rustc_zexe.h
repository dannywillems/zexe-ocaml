#ifndef RUSTC_ZEXE_INCLUDE_H
#define RUSTC_ZEXE_INCLUDE_H
#include <stdbool.h>

extern void rustc_zexe_generate_random_parameters(unsigned char *buffer_params);

extern void rustc_zexe_create_random_proof(unsigned char *buffer,
                                           const unsigned char *params,
                                           const unsigned char *a,
                                           const unsigned char *b);

extern bool rustc_zexe_verify_proof(const unsigned char *params,
                                    const unsigned char *proof,
                                    const unsigned char *c_input);

#endif

