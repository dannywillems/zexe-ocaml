use algebra::bls12_381::{Bls12_381, Fr, G1Affine, G2Affine};
use algebra_core::FromBytes;
use groth16::{Parameters, Proof, VerifyingKey};

use super::{
    H_LENGTH, LENGTH_FR_BYTES, LENGTH_G1_BYTES, LENGTH_G2_BYTES, LENGTH_PARAMS_BYTES,
    LENGTH_PROOF_BYTES, LENGTH_VERIFYING_KEY, NUM_INPUTS, NUM_PRIVATE_INPUTS, NUM_PUBLIC_INPUTS,
};

pub fn read_params(params: &[u8; LENGTH_PARAMS_BYTES]) -> Parameters<Bls12_381> {
    // Parameters
    let alpha_g1 = G1Affine::read(&params[0..LENGTH_G1_BYTES]).unwrap();
    let beta_g2 =
        G2Affine::read(&params[LENGTH_G1_BYTES..LENGTH_G1_BYTES + LENGTH_G2_BYTES]).unwrap();
    let gamma_g2 = G2Affine::read(
        &params[LENGTH_G1_BYTES + LENGTH_G2_BYTES..LENGTH_G1_BYTES + 2 * LENGTH_G2_BYTES],
    )
    .unwrap();
    let delta_g2 = G2Affine::read(
        &params[LENGTH_G1_BYTES + 2 * LENGTH_G2_BYTES..LENGTH_G1_BYTES + 3 * LENGTH_G2_BYTES],
    )
    .unwrap();
    let mut gamma_abc_g1 = vec![];
    for i in 0..NUM_PUBLIC_INPUTS {
        gamma_abc_g1.push(
            G1Affine::read(
                &params[(1 + i) * LENGTH_G1_BYTES + 3 * LENGTH_G2_BYTES
                    ..(1 + i + 1) * LENGTH_G1_BYTES + 3 * LENGTH_G2_BYTES],
            )
            .unwrap(),
        )
    }
    let beta_g1 =
        G1Affine::read(&params[LENGTH_VERIFYING_KEY..LENGTH_VERIFYING_KEY + LENGTH_G1_BYTES])
            .unwrap();
    let delta_g1 = G1Affine::read(
        &params[LENGTH_VERIFYING_KEY + LENGTH_G1_BYTES..LENGTH_VERIFYING_KEY + 2 * LENGTH_G1_BYTES],
    )
    .unwrap();

    let mut a_query = vec![];
    for i in 0..NUM_INPUTS {
        a_query.push(
            G1Affine::read(
                &params[LENGTH_VERIFYING_KEY + (2 + i) * LENGTH_G1_BYTES
                    ..LENGTH_VERIFYING_KEY + (2 + i + 1) * LENGTH_G1_BYTES],
            )
            .unwrap(),
        )
    }

    let mut b_g1 = vec![];
    for i in 0..NUM_INPUTS {
        b_g1.push(
            G1Affine::read(
                &params[LENGTH_VERIFYING_KEY + (2 + a_query.len() + i) * LENGTH_G1_BYTES
                    ..LENGTH_VERIFYING_KEY + (2 + a_query.len() + i + 1) * LENGTH_G1_BYTES],
            )
            .unwrap(),
        )
    }
    let mut b_g2 = vec![];
    for i in 0..NUM_INPUTS {
        b_g2.push(
            G2Affine::read(
                &params[LENGTH_VERIFYING_KEY
                    + (2 + a_query.len() + b_g1.len()) * LENGTH_G1_BYTES
                    + i * LENGTH_G2_BYTES
                    ..LENGTH_VERIFYING_KEY
                        + (2 + a_query.len() + b_g1.len()) * LENGTH_G1_BYTES
                        + (i + 1) * LENGTH_G2_BYTES],
            )
            .unwrap(),
        )
    }
    let mut h_query = vec![];
    for i in 0..H_LENGTH {
        h_query.push(
            G1Affine::read(
                &params[LENGTH_VERIFYING_KEY
                    + (2 + a_query.len() + b_g1.len() + i) * LENGTH_G1_BYTES
                    + b_g2.len() * LENGTH_G2_BYTES
                    ..LENGTH_VERIFYING_KEY
                        + (2 + a_query.len() + b_g1.len() + i + 1) * LENGTH_G1_BYTES
                        + b_g2.len() * LENGTH_G2_BYTES],
            )
            .unwrap(),
        )
    }
    let mut l_query = vec![];
    for i in 0..NUM_PRIVATE_INPUTS {
        l_query.push(
            G1Affine::read(
                &params[LENGTH_VERIFYING_KEY
                    + (2 + a_query.len() + b_g1.len() + h_query.len() + i) * LENGTH_G1_BYTES
                    + b_g2.len() * LENGTH_G2_BYTES
                    ..LENGTH_VERIFYING_KEY
                        + (2 + a_query.len() + b_g1.len() + h_query.len() + i + 1)
                            * LENGTH_G1_BYTES
                        + b_g2.len() * LENGTH_G2_BYTES],
            )
            .unwrap(),
        )
    }
    let vk = VerifyingKey::<Bls12_381> {
        alpha_g1,
        beta_g2,
        gamma_g2,
        delta_g2,
        gamma_abc_g1,
    };
    let params = Parameters::<Bls12_381> {
        vk,
        beta_g1,
        delta_g1,
        a_query,
        b_g1_query: b_g1,
        b_g2_query: b_g2,
        h_query,
        l_query,
    };
    params
}

pub fn read_fr(a: &[u8; LENGTH_FR_BYTES]) -> Fr {
    Fr::read(&a[..]).unwrap()
}

pub fn read_proof(buffer: &[u8; LENGTH_PROOF_BYTES]) -> Proof<Bls12_381> {
    let a = G1Affine::read(&buffer[..LENGTH_G1_BYTES]).unwrap();
    let b = G2Affine::read(&buffer[LENGTH_G1_BYTES..LENGTH_G1_BYTES + LENGTH_G2_BYTES]).unwrap();
    let c = G1Affine::read(&buffer[LENGTH_G1_BYTES + LENGTH_G2_BYTES..]).unwrap();
    Proof::<Bls12_381> { a, b, c }
}
