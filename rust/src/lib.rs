/* CHANGE THE FOLLOWING PARAMETERS DEPENDING TO MATCH THE CITCUIT */
const NUM_PUBLIC_INPUTS: usize = 2;
const NUM_PRIVATE_INPUTS: usize = 2;
const NUM_CONSTRAINTS: usize = 5;
const NUM_INPUTS: usize = NUM_PRIVATE_INPUTS + NUM_PUBLIC_INPUTS;
// This is next_power_of_two(NUM_PUBLIC_INPUTS + NUM_CONSTRAINTS) - 1
// Using next_powr_of_two statically is not yet possible in Rust.
const H_LENGTH: usize = 3;

/* UTILS */
const LENGTH_FR_BYTES: usize = 32;

// Add one for the byte encoding the zero
const LENGTH_G1_BYTES: usize = 96 + 1;
const LENGTH_G2_BYTES: usize = 192 + 1;
const LENGTH_VERIFYING_KEY: usize =

// vk
// -- alpha_g1
    1 * LENGTH_G1_BYTES
// -- beta_g2
    + 1 * LENGTH_G2_BYTES
// -- gamma_g2
    + 1 * LENGTH_G2_BYTES
// -- delta_g2
    + 1 * LENGTH_G2_BYTES
// gamma_abc_g1
    + NUM_PUBLIC_INPUTS * LENGTH_G1_BYTES;

// A groth16 proof is 2 G1 elements and 1 G2 element.
const LENGTH_PROOF_BYTES: usize = 2 * LENGTH_G1_BYTES + LENGTH_G2_BYTES;

const LENGTH_PARAMS_BYTES: usize = LENGTH_VERIFYING_KEY
// beta_g1
    + 1 * LENGTH_G1_BYTES
// delta_g1
    + 1 * LENGTH_G1_BYTES
// a_query
    + NUM_INPUTS * LENGTH_G1_BYTES
// b_g1
    + NUM_INPUTS * LENGTH_G1_BYTES
// b_g2
    + NUM_INPUTS * LENGTH_G2_BYTES
// h
    + H_LENGTH * LENGTH_G1_BYTES
// l
    + NUM_PRIVATE_INPUTS * LENGTH_G1_BYTES;

pub mod binding_groth16;
mod reader;
mod writer;
mod pedersen_hash;
