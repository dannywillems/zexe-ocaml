// use super::reader::{read_fr, read_params, read_proof};
// use super::writer::{write_params, write_proof};
// use super::{LENGTH_FR_BYTES, LENGTH_PARAMS_BYTES, LENGTH_PROOF_BYTES};

// use libc::c_uchar;

use algebra::{
    bytes::ToBytes,
    // bls12_381::Bls12_381,
    ed_on_bls12_381::EdwardsProjective as JubJub,
    ed_on_bls12_381::Fq as Fr,
    test_rng,
    to_bytes,
    Bls12_381,
};
use algebra_core::fields::Field;
// For CRH and Window. Do not use pedersen::{CRH, Window} to avoid conflict with
// names below.
use crypto_primitives::crh::pedersen;
use crypto_primitives::crh::pedersen::constraints::{CRHGadget, CRHParametersVar};
use crypto_primitives::crh::{FixedLengthCRH, FixedLengthCRHGadget};
use groth16::{
    create_random_proof, generate_random_parameters, prepare_verifying_key, verify_proof,
};
use r1cs_core::{lc, ConstraintSynthesizer, ConstraintSystem, ConstraintSystemRef, SynthesisError};
use r1cs_std::alloc::AllocVar;
use r1cs_std::bits::uint8::UInt8;
use r1cs_std::ed_on_bls12_381::EdwardsVar;
use r1cs_std::R1CSVar;

struct PedersenHash {
    input: Option<[u8; 128]>,
}

// Window for the CRH. Just took the value from pedersen::CRH::constraints. To
// change.
#[derive(Clone)]
pub struct Window;

impl pedersen::Window for Window {
    const WINDOW_SIZE: usize = 128;
    const NUM_WINDOWS: usize = 8;
}

impl ConstraintSynthesizer<Fr> for PedersenHash {
    fn generate_constraints(self, cs: ConstraintSystemRef<Fr>) -> Result<(), SynthesisError> {
        // Use a test RNG
        let rng = &mut test_rng();
        let input = self.input.unwrap();

        // Setting up the circuit with the correct bytes.
        // Adding a witness input for each bytes of the input.
        let mut input_var = vec![];
        for byte in input.iter() {
            input_var.push(UInt8::new_witness(cs.clone(), || Ok(byte)).unwrap());
        }

        // Parameters could be a parameters on in the structure.
        let parameters = pedersen::CRH::<JubJub, Window>::setup(rng).unwrap();
        let parameters_var = CRHParametersVar::<JubJub, EdwardsVar>::new_constant(
            r1cs_core::ns!(cs, "CRH Parameters"),
            &parameters,
        )
        .unwrap();

        let result_var =
            CRHGadget::<JubJub, EdwardsVar, Window>::evaluate(&parameters_var, &input_var).unwrap();
        // println!("{:?}", result_var.value());

        // assert_eq!(computed_result, result_var.value().unwrap());
        assert!(cs.is_satisfied().unwrap());
        Ok(())
    }
}

fn main() {
    let rng = &mut test_rng();

    let h = PedersenHash {
        input: Some([0; 128]),
    };

    // Generate parameters for Pedersen and compute expected result.
    let parameters = pedersen::CRH::<JubJub, Window>::setup(rng).unwrap();
    let computed_result =
        pedersen::CRH::<JubJub, Window>::evaluate(&parameters, &h.input.unwrap()).unwrap();
    let computed_result_bytes = to_bytes![computed_result];

    let cs = ConstraintSystem::<Fr>::new_ref();
    h.generate_constraints(cs);

    // Use a test RNG
    let rng = &mut test_rng();

    let params =
        generate_random_parameters::<Bls12_381, _, _>(PedersenHash { input: None }, rng).unwrap();

    // // Parameters
    let proof = create_random_proof(
        PedersenHash {
            input: Some([0; 128]),
        },
        &params,
        rng,
    )
    .unwrap();

    let pvk = prepare_verifying_key(&params.vk);

    // let hash_bits = multipack::bytes_to_bits_le(&hash);
    // let inputs = multipack::compute_multipacking::<Bls12>(&hash_bits);
    // verify_proof(&pvk, &proof, &[computed_result]).unwrap();
}
