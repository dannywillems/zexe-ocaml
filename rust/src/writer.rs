use super::{
    LENGTH_G1_BYTES, LENGTH_G2_BYTES, LENGTH_PARAMS_BYTES, LENGTH_PROOF_BYTES, LENGTH_VERIFYING_KEY,
};
use algebra::bls12_381::Bls12_381;
use algebra_core::ToBytes;
use groth16::{Parameters, Proof};

pub fn write_params(buffer: &mut [u8; LENGTH_PARAMS_BYTES], params: Parameters<Bls12_381>) {
    params.vk.write(&mut buffer[..]).unwrap();
    params
        .beta_g1
        .write(&mut buffer[LENGTH_VERIFYING_KEY..LENGTH_VERIFYING_KEY + LENGTH_G1_BYTES])
        .unwrap();
    params
        .delta_g1
        .write(
            &mut buffer[LENGTH_VERIFYING_KEY + LENGTH_G1_BYTES
                ..LENGTH_VERIFYING_KEY + 2 * LENGTH_G1_BYTES],
        )
        .unwrap();
    for (i, v) in params.a_query.iter().enumerate() {
        v.write(
            &mut buffer[LENGTH_VERIFYING_KEY + (2 + i) * LENGTH_G1_BYTES
                ..LENGTH_VERIFYING_KEY + (2 + i + 1) * LENGTH_G1_BYTES],
        )
        .unwrap();
    }
    for (i, v) in params.b_g1_query.iter().enumerate() {
        v.write(
            &mut buffer[LENGTH_VERIFYING_KEY + (2 + params.a_query.len() + i) * LENGTH_G1_BYTES
                ..LENGTH_VERIFYING_KEY + (2 + params.a_query.len() + i + 1) * LENGTH_G1_BYTES],
        )
        .unwrap()
    }
    for (i, v) in params.b_g2_query.iter().enumerate() {
        v.write(
            &mut buffer[LENGTH_VERIFYING_KEY
                + (2 + params.a_query.len() + params.b_g1_query.len()) * LENGTH_G1_BYTES
                + i * LENGTH_G2_BYTES
                ..LENGTH_VERIFYING_KEY
                    + (2 + params.a_query.len() + params.b_g1_query.len()) * LENGTH_G1_BYTES
                    + (i + 1) * LENGTH_G2_BYTES],
        )
        .unwrap()
    }
    for (i, v) in params.h_query.iter().enumerate() {
        v.write(
            &mut buffer[LENGTH_VERIFYING_KEY
                + (2 + params.a_query.len() + params.b_g1_query.len() + i) * LENGTH_G1_BYTES
                + params.b_g2_query.len() * LENGTH_G2_BYTES
                ..LENGTH_VERIFYING_KEY
                    + (2 + params.a_query.len() + params.b_g1_query.len() + i + 1)
                        * LENGTH_G1_BYTES
                    + params.b_g2_query.len() * LENGTH_G2_BYTES],
        )
        .unwrap()
    }
    for (i, v) in params.l_query.iter().enumerate() {
        v.write(
            &mut buffer[LENGTH_VERIFYING_KEY
                + (2 + params.a_query.len() + params.b_g1_query.len() + params.h_query.len() + i)
                    * LENGTH_G1_BYTES
                + params.b_g2_query.len() * LENGTH_G2_BYTES
                ..LENGTH_VERIFYING_KEY
                    + (2 + params.a_query.len()
                        + params.b_g1_query.len()
                        + params.h_query.len()
                        + i
                        + 1)
                        * LENGTH_G1_BYTES
                    + params.b_g2_query.len() * LENGTH_G2_BYTES],
        )
        .unwrap()
    }
}

pub fn write_proof(buffer: &mut [u8; LENGTH_PROOF_BYTES], proof: Proof<Bls12_381>) {
    proof.a.write(&mut buffer[..LENGTH_G1_BYTES]).unwrap();
    proof
        .b
        .write(&mut buffer[LENGTH_G1_BYTES..LENGTH_G2_BYTES + LENGTH_G1_BYTES])
        .unwrap();
    proof
        .c
        .write(&mut buffer[LENGTH_G1_BYTES + LENGTH_G2_BYTES..])
        .unwrap();
}
