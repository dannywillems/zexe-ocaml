use super::reader::{read_fr, read_params, read_proof};
use super::writer::{write_params, write_proof};
use algebra::bls12_381::Bls12_381;
use algebra_core::Field;
use groth16::{
    create_random_proof, generate_random_parameters, prepare_verifying_key, verify_proof,
};
use libc::c_uchar;
use r1cs_core::{lc, ConstraintSynthesizer, ConstraintSystemRef, SynthesisError};
use rand::rngs::OsRng;

use super::{LENGTH_FR_BYTES, LENGTH_PARAMS_BYTES, LENGTH_PROOF_BYTES};

struct MySillyCircuit<F: Field> {
    a: Option<F>,
    b: Option<F>,
}

impl<ConstraintF: Field> ConstraintSynthesizer<ConstraintF> for MySillyCircuit<ConstraintF> {
    fn generate_constraints(
        self,
        cs: ConstraintSystemRef<ConstraintF>,
    ) -> Result<(), SynthesisError> {
        let a = cs.new_witness_variable(|| self.a.ok_or(SynthesisError::AssignmentMissing))?;
        let b = cs.new_witness_variable(|| self.b.ok_or(SynthesisError::AssignmentMissing))?;
        let c = cs.new_input_variable(|| {
            let mut a = self.a.ok_or(SynthesisError::AssignmentMissing)?;
            let b = self.b.ok_or(SynthesisError::AssignmentMissing)?;

            a.mul_assign(&b);
            Ok(a)
        })?;

        cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;
        // cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;
        // cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;
        // cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;
        // cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;
        // cs.enforce_constraint(lc!() + a, lc!() + b, lc!() + c)?;

        Ok(())
    }
}

#[no_mangle]
pub extern "C" fn rustc_zexe_generate_random_parameters(
    buffer: *mut [c_uchar; LENGTH_PARAMS_BYTES],
) {
    let params = generate_random_parameters::<Bls12_381, _, _>(
        MySillyCircuit { a: None, b: None },
        &mut OsRng,
    )
    .unwrap();

    let buffer = unsafe { &mut *buffer };
    write_params(buffer, params)
}

#[no_mangle]
pub extern "C" fn rustc_zexe_create_random_proof(
    buffer: *mut [c_uchar; LENGTH_PROOF_BYTES],
    params: *const [c_uchar; LENGTH_PARAMS_BYTES],
    a: *const [c_uchar; LENGTH_FR_BYTES],
    b: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let a = read_fr(unsafe { &*a });
    let b = read_fr(unsafe { &*b });

    let params = unsafe { &*params };
    let params = read_params(&params);

    // Parameters
    let proof = create_random_proof(
        MySillyCircuit {
            a: Some(a),
            b: Some(b),
        },
        &params,
        &mut OsRng,
    )
    .unwrap();

    let buffer = unsafe { &mut *buffer };
    write_proof(buffer, proof);
}

#[no_mangle]
pub extern "C" fn rustc_zexe_verify_proof(
    params: *const [c_uchar; LENGTH_PARAMS_BYTES],
    proof: *const [c_uchar; LENGTH_PROOF_BYTES],
    c_input: *const [c_uchar; LENGTH_FR_BYTES],
) -> bool {
    let params = unsafe { &*params };
    let params = read_params(&params);
    let pvk = prepare_verifying_key(&params.vk);

    let proof = read_proof(unsafe { &*proof });
    let c_input = read_fr(unsafe { &*c_input });

    verify_proof(&pvk, &proof, &[c_input]).unwrap()
}
