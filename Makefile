build-rust:
	./build_deps.sh

build-ocaml:
	cd ocaml
	eval $(opam env)
	dune build

run-test: build-rust
	cd ocaml
	eval $(opam env)
	dune clean
	dune build @runtest
