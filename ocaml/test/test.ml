let rec repeat n f =
  match n with
  | n when n > 0 ->
      f () ;
      repeat (n - 1) f
  | _ -> fun () -> ()

module Conversion = struct
  let test_g1_zero () =
    assert (
      Bls12_381.G1.Uncompressed.eq
        Bls12_381.G1.Uncompressed.zero
        (Zexe.Convert.g1_of_zexe
           (Zexe.Convert.g1_to_zexe Bls12_381.G1.Uncompressed.zero)) )

  let test_g2_zero () =
    assert (
      Bls12_381.G2.Uncompressed.eq
        Bls12_381.G2.Uncompressed.zero
        (Zexe.Convert.g2_of_zexe
           (Zexe.Convert.g2_to_zexe Bls12_381.G2.Uncompressed.zero)) )

  let test_g1_one () =
    assert (
      Bls12_381.G1.Uncompressed.eq
        Bls12_381.G1.Uncompressed.one
        (Zexe.Convert.g1_of_zexe
           (Zexe.Convert.g1_to_zexe Bls12_381.G1.Uncompressed.one)) )

  let test_g2_one () =
    assert (
      Bls12_381.G2.Uncompressed.eq
        Bls12_381.G2.Uncompressed.one
        (Zexe.Convert.g2_of_zexe
           (Zexe.Convert.g2_to_zexe Bls12_381.G2.Uncompressed.one)) )

  let test_g1_random () =
    let r = Bls12_381.G1.Uncompressed.random () in
    assert (
      Bls12_381.G1.Uncompressed.eq
        r
        (Zexe.Convert.g1_of_zexe (Zexe.Convert.g1_to_zexe r)) )

  let test_g2_random () =
    let r = Bls12_381.G2.Uncompressed.random () in
    assert (
      Bls12_381.G2.Uncompressed.eq
        r
        (Zexe.Convert.g2_of_zexe (Zexe.Convert.g2_to_zexe r)) )

  let test_g1_with_addition () =
    let a = Bls12_381.G1.Uncompressed.random () in
    let b = Bls12_381.G1.Uncompressed.random () in
    let a_back = Zexe.Convert.(g1_of_zexe (g1_to_zexe a)) in
    let b_back = Zexe.Convert.(g1_of_zexe (g1_to_zexe b)) in
    assert (Bls12_381.G1.Uncompressed.(eq (add a b) (add a_back b_back))) ;
    assert (Bls12_381.G1.Uncompressed.(eq (add a b) (add a b_back))) ;
    assert (Bls12_381.G1.Uncompressed.(eq (add a b) (add a_back b)))

  let test_g2_with_addition () =
    let a = Bls12_381.G2.Uncompressed.random () in
    let b = Bls12_381.G2.Uncompressed.random () in
    let a_back = Zexe.Convert.(g2_of_zexe (g2_to_zexe a)) in
    let b_back = Zexe.Convert.(g2_of_zexe (g2_to_zexe b)) in
    assert (Bls12_381.G2.Uncompressed.(eq (add a b) (add a_back b_back))) ;
    assert (Bls12_381.G2.Uncompressed.(eq (add a b) (add a b_back))) ;
    assert (Bls12_381.G2.Uncompressed.(eq (add a b) (add a_back b)))
end

let test_conversion_vk_and_params () =
  let params = Zexe.Circuit.setup () in
  assert (
    params.vk
    = Zexe.Parameters.Vk.of_bytes_exn (Zexe.Parameters.Vk.to_bytes params.vk) ) ;
  let result = Zexe.Parameters.of_bytes_exn (Zexe.Parameters.to_bytes params) in
  assert (params = result)

let test_proof_and_verif () =
  let params = Zexe.Circuit.setup () in
  (* Correct proof *)
  let a = Bls12_381.Fr.random () in
  let b = Bls12_381.Fr.random () in
  let proof = Zexe.Circuit.(create_proof { a; b } params) in
  assert (Zexe.Circuit.(verify_proof params proof (Bls12_381.Fr.mul a b))) ;
  (* Incorrect proof *)
  assert (not Zexe.Circuit.(verify_proof params proof Bls12_381.Fr.zero))

let () =
  let open Alcotest in
  run
    "ZEXE"
    [ ( "Conversion",
        [ test_case "g1 random" `Quick (repeat 1000 Conversion.test_g1_random);
          test_case "g1 zero" `Quick Conversion.test_g1_zero;
          test_case "g1 one" `Quick Conversion.test_g1_one;
          test_case "g2 zero" `Quick Conversion.test_g2_zero;
          test_case "g2 one" `Quick Conversion.test_g2_one;
          test_case "g2 random" `Quick (repeat 1000 Conversion.test_g2_random);
          test_case
            "g1 addition"
            `Quick
            (repeat 1000 Conversion.test_g1_with_addition);
          test_case
            "g2 addition"
            `Quick
            (repeat 1000 Conversion.test_g2_with_addition);
          test_case
            "Vk and params"
            `Quick
            (repeat 1000 test_conversion_vk_and_params) ] );
      ( "Full scenario",
        [test_case "Proof and verif" `Quick (repeat 100 test_proof_and_verif)]
      ) ]
