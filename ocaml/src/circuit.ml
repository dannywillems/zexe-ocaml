(* print_endline @@ String.concat ", " (List.map (fun c -> string_of_int (int_of_char c)) (List.of_seq (Bytes.to_seq buffer))); *)

open Bls12_381
open Convert
module G1 = Bls12_381.G1.Uncompressed
module G2 = Bls12_381.G2.Uncompressed

type t = { a : Fr.t; b : Fr.t }

type proof = { a : G1.t; b : G2.t; c : G1.t }

let proof_size_in_bytes =
  zexe_g1_size_in_bytes + zexe_g2_size_in_bytes + zexe_g1_size_in_bytes

module Stubs = Rustc_zexe_bindings.Bindings (Rustc_zexe_stubs)

let setup _circuit =
  let open Bls12_381 in
  let module G1 = G1.Uncompressed in
  let module G2 = G2.Uncompressed in
  let buffer = Bytes.make Parameters.size_in_bytes '\000' in
  Stubs.generate_random_parameters (Ctypes.ocaml_bytes_start buffer) ;
  Parameters.of_bytes_exn buffer

let create_proof (circuit : t) params : proof =
  let a_bytes = Fr.to_bytes circuit.a in
  let b_bytes = Fr.to_bytes circuit.b in
  let params_bytes = Parameters.to_bytes params in
  let buffer = Bytes.make proof_size_in_bytes '\000' in
  Stubs.create_random_proof
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start params_bytes)
    (Ctypes.ocaml_bytes_start a_bytes)
    (Ctypes.ocaml_bytes_start b_bytes) ;
  let a = g1_of_zexe (Bytes.sub buffer 0 zexe_g1_size_in_bytes) in
  let b =
    g2_of_zexe (Bytes.sub buffer zexe_g1_size_in_bytes zexe_g2_size_in_bytes)
  in
  let c =
    g1_of_zexe
      (Bytes.sub
         buffer
         (zexe_g1_size_in_bytes + zexe_g2_size_in_bytes)
         zexe_g1_size_in_bytes)
  in
  { a; b; c }

let verify_proof params proof input =
  let params_bytes = Parameters.to_bytes params in
  let proof_bytes = Bytes.make proof_size_in_bytes '\000' in
  Bytes.blit (g1_to_zexe proof.a) 0 proof_bytes 0 zexe_g1_size_in_bytes ;
  Bytes.blit
    (g2_to_zexe proof.b)
    0
    proof_bytes
    zexe_g1_size_in_bytes
    zexe_g2_size_in_bytes ;
  Bytes.blit
    (g1_to_zexe proof.c)
    0
    proof_bytes
    (zexe_g1_size_in_bytes + zexe_g2_size_in_bytes)
    zexe_g1_size_in_bytes ;
  let input_bytes = Fr.to_bytes input in
  Stubs.verify_proof
    (Ctypes.ocaml_bytes_start params_bytes)
    (Ctypes.ocaml_bytes_start proof_bytes)
    (Ctypes.ocaml_bytes_start input_bytes)
