open Convert
module G1 = Bls12_381.G1.Uncompressed
module G2 = Bls12_381.G2.Uncompressed

let num_constraints = 1

let num_auxiliary_inputs = 2

let num_public_inputs = 2

let num_inputs = num_public_inputs + num_auxiliary_inputs

let next_power_of_two n =
  assert (n > 0);
  let rec aux i n = if n = 1 then i + 1 else aux (i + 1) (n / 2) in
  let res = 1 lsl (aux 0 n) in
  if n * 2 = res then n else res

let h_length = next_power_of_two (num_public_inputs + num_constraints) - 1

module Vk = struct
  (* [alpha]_{1} * [beta]_{2} +
     \sum{i = 0}^{l} (a_i * [beta u_i(x) + alpha v_i(x) + w_i(x)]_{1} * [gamma^{-1}]_{1} * [gamma]_{2}) +
     [C]_{1} * [delta]_{2} *)

  type t =
    { alpha_g1 : G1.t;
      beta_g2 : G2.t;
      gamma_g2 : G2.t;
      delta_g2 : G2.t;
      gamma_abc_g1 : G1.t list
    }

  (* specific for this circuit *)
  let size_in_bytes =
    (1 * zexe_g1_size_in_bytes)
    + (1 * zexe_g2_size_in_bytes)
    + (1 * zexe_g2_size_in_bytes)
    + (1 * zexe_g2_size_in_bytes)
    + (num_public_inputs * zexe_g1_size_in_bytes)

  let of_bytes_exn buffer =
    assert (Bytes.length buffer = size_in_bytes) ;
    let alpha_g1 = g1_of_zexe (Bytes.sub buffer 0 zexe_g1_size_in_bytes) in
    let beta_g2 =
      g2_of_zexe (Bytes.sub buffer zexe_g1_size_in_bytes zexe_g2_size_in_bytes)
    in
    let gamma_g2 =
      g2_of_zexe
        (Bytes.sub
           buffer
           (zexe_g1_size_in_bytes + zexe_g2_size_in_bytes)
           zexe_g2_size_in_bytes)
    in
    let delta_g2 =
      g2_of_zexe
        (Bytes.sub
           buffer
           (zexe_g1_size_in_bytes + (2 * zexe_g2_size_in_bytes))
           zexe_g2_size_in_bytes)
    in
    let gamma_abc_g1 =
      List.init num_public_inputs (fun i ->
          g1_of_zexe
            (Bytes.sub
               buffer
               (((1 + i) * zexe_g1_size_in_bytes) + (3 * zexe_g2_size_in_bytes))
               zexe_g1_size_in_bytes))
    in
    { alpha_g1; beta_g2; gamma_g2; delta_g2; gamma_abc_g1 }

  let to_bytes vk =
    let buffer = Bytes.make size_in_bytes '\000' in
    Bytes.blit (g1_to_zexe vk.alpha_g1) 0 buffer 0 zexe_g1_size_in_bytes ;
    Bytes.blit
      (g2_to_zexe vk.beta_g2)
      0
      buffer
      zexe_g1_size_in_bytes
      zexe_g2_size_in_bytes ;
    Bytes.blit
      (g2_to_zexe vk.gamma_g2)
      0
      buffer
      (zexe_g1_size_in_bytes + zexe_g2_size_in_bytes)
      zexe_g2_size_in_bytes ;
    Bytes.blit
      (g2_to_zexe vk.delta_g2)
      0
      buffer
      (zexe_g1_size_in_bytes + (2 * zexe_g2_size_in_bytes))
      zexe_g2_size_in_bytes ;
    List.iteri
      (fun i v ->
        Bytes.blit
          (g1_to_zexe v)
          0
          buffer
          (((1 + i) * zexe_g1_size_in_bytes) + (3 * zexe_g2_size_in_bytes))
          zexe_g1_size_in_bytes)
      vk.gamma_abc_g1 ;
    buffer
end

module Pvk = struct
  type t = int
end

type t =
  { vk : Vk.t;
    beta_g1 : G1.t;
    delta_g1 : G1.t;
    (*
   QAP "A" polynomials evaluated at tau in the Lagrange basis. Never contains
   points at infinity: polynomials that evaluate to zero are omitted from
   the CRS and the prover can deterministically skip their evaluation.
  *)
    a : G1.t list;
    (*
    QAP "B" polynomials evaluated at tau in the Lagrange basis. Needed in
    G1 and G2 for C/B queries, respectively. Never contains points at
    infinity for the same reason as the "A" polynomials.
  *)
    b_g1 : G1.t list;
    b_g2 : G2.t list;
    (*
    Elements of the form ((tau^i * t(tau)) / delta) for i between 0 and
    m-2 inclusive. Never contains points at infinity.
  *)
    h : G1.t list;
    (*
    Elements of the form (beta * u_i(tau) + alpha v_i(tau) + w_i(tau)) / delta
    for all auxiliary inputs. Variables can never be unconstrained, so this
    never contains points at infinity.
  *)
    l : G1.t list
  }

(* specific for the example circuit *)
let size_in_bytes =
  Vk.size_in_bytes
  + (1 * zexe_g1_size_in_bytes)
  + (1 * zexe_g1_size_in_bytes)
  (* a *)
  + num_inputs * zexe_g1_size_in_bytes
  (* b_g1 *)
  + num_inputs * zexe_g1_size_in_bytes
  (* b_g2 *)
  + num_inputs * zexe_g2_size_in_bytes
  (* h *)
  + (h_length * zexe_g1_size_in_bytes)
  (* l *)
  + num_auxiliary_inputs * zexe_g1_size_in_bytes

let vk_of_t p = p.vk

let pvk_of_vk vk =
  ignore vk ;
  failwith "not implemented"

let of_bytes_exn buffer =
  let vk = Vk.of_bytes_exn (Bytes.sub buffer 0 Vk.size_in_bytes) in
  let beta_g1 =
    g1_of_zexe (Bytes.sub buffer Vk.size_in_bytes zexe_g1_size_in_bytes)
  in
  let delta_g1 =
    g1_of_zexe
      (Bytes.sub
         buffer
         (Vk.size_in_bytes + zexe_g1_size_in_bytes)
         zexe_g1_size_in_bytes)
  in
  let a =
    List.init num_inputs (fun i ->
        g1_of_zexe
          (Bytes.sub
             buffer
             (Vk.size_in_bytes + ((2 + i) * zexe_g1_size_in_bytes))
             zexe_g1_size_in_bytes))
  in
  let b_g1 =
    List.init num_inputs (fun i ->
        let bytes =
          Bytes.sub
            buffer
            ( Vk.size_in_bytes
            + ((2 + num_inputs + i) * zexe_g1_size_in_bytes) )
            zexe_g1_size_in_bytes
        in
        g1_of_zexe bytes)
  in
  let b_g2 =
    List.init num_inputs (fun i ->
        g2_of_zexe
          (Bytes.sub
             buffer
             ( Vk.size_in_bytes
             + ((2 + (2 * num_inputs)) * zexe_g1_size_in_bytes)
             + (i * zexe_g2_size_in_bytes) )
             zexe_g2_size_in_bytes))
  in
  let h =
    List.init h_length (fun i ->
        g1_of_zexe
          (Bytes.sub
             buffer
             ( Vk.size_in_bytes
             + ((2 + 2 * num_inputs + i) * zexe_g1_size_in_bytes)
             + (num_inputs * zexe_g2_size_in_bytes) )
             zexe_g1_size_in_bytes))
  in
  let l =
    List.init num_auxiliary_inputs (fun i ->
        g1_of_zexe
          (Bytes.sub
             buffer
             ( Vk.size_in_bytes
             + (2 + (2 * num_inputs) + h_length + i)
               * zexe_g1_size_in_bytes
             + (num_inputs * zexe_g2_size_in_bytes) )
             zexe_g1_size_in_bytes))
  in
  { vk; beta_g1; delta_g1; a; b_g1; b_g2; h; l }

let to_bytes params =
  let buffer = Bytes.make size_in_bytes '\000' in
  Bytes.blit (Vk.to_bytes params.vk) 0 buffer 0 Vk.size_in_bytes ;
  Bytes.blit
    (g1_to_zexe params.beta_g1)
    0
    buffer
    Vk.size_in_bytes
    zexe_g1_size_in_bytes ;
  Bytes.blit
    (g1_to_zexe params.delta_g1)
    0
    buffer
    (Vk.size_in_bytes + zexe_g1_size_in_bytes)
    zexe_g1_size_in_bytes ;
  List.iteri
    (fun i v ->
      Bytes.blit
        (g1_to_zexe v)
        0
        buffer
        (Vk.size_in_bytes + ((2 + i) * zexe_g1_size_in_bytes))
        zexe_g1_size_in_bytes)
    params.a ;
  List.iteri
    (fun i v ->
      Bytes.blit
        (g1_to_zexe v)
        0
        buffer
        ( Vk.size_in_bytes
        + ((2 + List.length params.a + i) * zexe_g1_size_in_bytes) )
        zexe_g1_size_in_bytes)
    params.b_g1 ;
  List.iteri
    (fun i v ->
      Bytes.blit
        (g2_to_zexe v)
        0
        buffer
        ( Vk.size_in_bytes
        + (2 + List.length params.a + List.length params.b_g1)
          * zexe_g1_size_in_bytes
        + (i * zexe_g2_size_in_bytes) )
        zexe_g2_size_in_bytes)
    params.b_g2 ;
  List.iteri
    (fun i v ->
      Bytes.blit
        (g1_to_zexe v)
        0
        buffer
        ( Vk.size_in_bytes
        + (2 + List.length params.a + List.length params.b_g1 + i)
          * zexe_g1_size_in_bytes
        + (List.length params.b_g2 * zexe_g2_size_in_bytes) )
        zexe_g1_size_in_bytes)
    params.h ;
  List.iteri
    (fun i v ->
      Bytes.blit
        (g1_to_zexe v)
        0
        buffer
        ( Vk.size_in_bytes
        + ( 2 + List.length params.a + List.length params.b_g1
          + List.length params.h + i )
          * zexe_g1_size_in_bytes
        + (List.length params.b_g2 * zexe_g2_size_in_bytes) )
        zexe_g1_size_in_bytes)
    params.l ;
  buffer
