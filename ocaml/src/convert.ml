module G1 = Bls12_381.G1.Uncompressed
module G2 = Bls12_381.G2.Uncompressed

let zexe_g1_size_in_bytes = G1.size_in_bytes + 1

let zexe_g2_size_in_bytes = G2.size_in_bytes + 1

(* zexe uses a little endian encoding and use one additional byte for the zero.
   bellman (aka ocaml-bls12-381) uses big endian and use the first byte to encode
   the zero *)
let g1_of_zexe zexe_bytes =
  assert (Bytes.length zexe_bytes = G1.size_in_bytes + 1) ;
  if Bytes.get zexe_bytes G1.size_in_bytes = '\001' then G1.zero
  else
    let x_le = Bytes.sub zexe_bytes 0 (G1.size_in_bytes / 2) in
    let y_le =
      Bytes.sub zexe_bytes (G1.size_in_bytes / 2) (G1.size_in_bytes / 2)
    in
    let x_be =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_le))))
    in
    let y_be =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_le))))
    in
    let buffer_be = Bytes.make G1.size_in_bytes '\000' in
    Bytes.blit x_be 0 buffer_be 0 (G1.size_in_bytes / 2) ;
    Bytes.blit y_be 0 buffer_be (G1.size_in_bytes / 2) (G1.size_in_bytes / 2) ;
    G1.of_bytes_exn buffer_be

let g2_of_zexe zexe_bytes =
  assert (Bytes.length zexe_bytes = G2.size_in_bytes + 1) ;
  if Bytes.get zexe_bytes G2.size_in_bytes = '\001' then G2.zero
  else
    let x_le_c0 = Bytes.sub zexe_bytes 0 (G2.size_in_bytes / 4) in
    let x_le_c1 =
      Bytes.sub zexe_bytes (G2.size_in_bytes / 4) (G2.size_in_bytes / 4)
    in
    let y_le_c0 =
      Bytes.sub zexe_bytes (G2.size_in_bytes / 2) (G2.size_in_bytes / 4)
    in
    let y_le_c1 =
      Bytes.sub zexe_bytes (3 * G2.size_in_bytes / 4) (G2.size_in_bytes / 4)
    in
    let x_be_c0 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_le_c0))))
    in
    let x_be_c1 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_le_c1))))
    in
    let y_be_c0 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_le_c0))))
    in
    let y_be_c1 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_le_c1))))
    in
    let buffer_be = Bytes.make G2.size_in_bytes '\000' in
    Bytes.blit x_be_c1 0 buffer_be 0 (G2.size_in_bytes / 4) ;
    Bytes.blit x_be_c0 0 buffer_be (G2.size_in_bytes / 4) (G2.size_in_bytes / 4) ;
    Bytes.blit y_be_c1 0 buffer_be (G2.size_in_bytes / 2) (G2.size_in_bytes / 4) ;
    Bytes.blit
      y_be_c0
      0
      buffer_be
      (3 * G2.size_in_bytes / 4)
      (G2.size_in_bytes / 4) ;
    G2.of_bytes_exn buffer_be

let g1_to_zexe g1 =
  let buffer = Bytes.make (G1.size_in_bytes + 1) '\000' in
  if G1.is_zero g1 then (
    Bytes.set buffer G1.size_in_bytes '\001' ;
    buffer )
  else
    let g1_bytes = G1.to_bytes g1 in
    let x_be = Bytes.sub g1_bytes 0 (G1.size_in_bytes / 2) in
    let y_be =
      Bytes.sub g1_bytes (G1.size_in_bytes / 2) (G1.size_in_bytes / 2)
    in
    let x_le =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_be))))
    in
    let y_le =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_be))))
    in
    Bytes.blit x_le 0 buffer 0 (G1.size_in_bytes / 2) ;
    Bytes.blit y_le 0 buffer (G1.size_in_bytes / 2) (G1.size_in_bytes / 2) ;
    buffer

let g2_to_zexe g2 =
  let buffer = Bytes.make (G2.size_in_bytes + 1) '\000' in
  if G2.is_zero g2 then (
    Bytes.set buffer G2.size_in_bytes '\001' ;
    buffer )
  else
    let g2_bytes = G2.to_bytes g2 in
    let x_be_c1 = Bytes.sub g2_bytes 0 (G2.size_in_bytes / 4) in
    let x_be_c0 =
      Bytes.sub g2_bytes (G2.size_in_bytes / 4) (G2.size_in_bytes / 4)
    in
    let y_be_c1 =
      Bytes.sub g2_bytes (G2.size_in_bytes / 2) (G2.size_in_bytes / 4)
    in
    let y_be_c0 =
      Bytes.sub g2_bytes (3 * G2.size_in_bytes / 4) (G2.size_in_bytes / 4)
    in
    let x_le_c1 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_be_c1))))
    in
    let x_le_c0 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq x_be_c0))))
    in
    let y_le_c1 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_be_c1))))
    in
    let y_le_c0 =
      Bytes.of_seq (List.to_seq (List.rev (List.of_seq (Bytes.to_seq y_be_c0))))
    in
    Bytes.blit x_le_c0 0 buffer 0 (G2.size_in_bytes / 4) ;
    Bytes.blit x_le_c1 0 buffer (G2.size_in_bytes / 4) (G2.size_in_bytes / 4) ;
    Bytes.blit y_le_c0 0 buffer (G2.size_in_bytes / 2) (G2.size_in_bytes / 4) ;
    Bytes.blit y_le_c1 0 buffer (3 * G2.size_in_bytes / 4) (G2.size_in_bytes / 4) ;
    buffer
