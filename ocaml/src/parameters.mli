module G1 = Bls12_381.G1.Uncompressed
module G2 = Bls12_381.G2.Uncompressed

module Vk : sig
  type t =
    { alpha_g1 : G1.t;
      beta_g2 : G2.t;
      gamma_g2 : G2.t;
      delta_g2 : G2.t;
      gamma_abc_g1 : G1.t list
    }

  val size_in_bytes : int

  val of_bytes_exn : Bytes.t -> t

  val to_bytes : t -> Bytes.t
end

module Pvk : sig
  type t

  (* val size_in_bytes : int
   * 
   * val of_bytes_exn : Bytes.t -> t
   * 
   * val to_bytes : t -> Bytes.t *)
end

type t =
  { vk : Vk.t;
    beta_g1 : G1.t;
    delta_g1 : G1.t;
    (*
   QAP "A" polynomials evaluated at tau in the Lagrange basis. Never contains
   points at infinity: polynomials that evaluate to zero are omitted from
   the CRS and the prover can deterministically skip their evaluation.
  *)
    a : G1.t list;
    (*
    QAP "B" polynomials evaluated at tau in the Lagrange basis. Needed in
    G1 and G2 for C/B queries, respectively. Never contains points at
    infinity for the same reason as the "A" polynomials.
  *)
    b_g1 : G1.t list;
    b_g2 : G2.t list;
    (*
    Elements of the form ((tau^i * t(tau)) / delta) for i between 0 and
    m-2 inclusive. Never contains points at infinity.
  *)
    h : G1.t list;
    (*
    Elements of the form (beta * u_i(tau) + alpha v_i(tau) + w_i(tau)) / delta
    for all auxiliary inputs. Variables can never be unconstrained, so this
    never contains points at infinity.
  *)
    l : G1.t list
  }

val size_in_bytes : int

val of_bytes_exn : Bytes.t -> t

val to_bytes : t -> Bytes.t

val vk_of_t : t -> Vk.t

val pvk_of_vk : Vk.t -> Pvk.t
