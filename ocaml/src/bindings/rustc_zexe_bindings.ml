open Ctypes

module Bindings (F : Cstubs.FOREIGN) = struct
  open F

  let generate_random_parameters =
    foreign
      "rustc_zexe_generate_random_parameters"
      (ocaml_bytes @-> returning void)

  let create_random_proof =
    foreign
      "rustc_zexe_create_random_proof"
      (ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> returning void)

  let verify_proof =
    foreign
      "rustc_zexe_verify_proof"
      (ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> returning bool)
end
